<?php include_once 'layout/header.php'; ?>
<section id="login">
    <div class="container">
        <div class="row">
            <div class="col s12 title_login">
                <h1>
                    <img src="assets/images/logo.png" alt="logo" class="logo_login">
                </h1>
            </div>

            <div class="col s12 subtitle">
                <div class="col s4 line_white"></div>
                <div class="col s4 subtitle_login">Login</div>
                <div class="col s4 line_white"></div>
            </div>
        </div>



        <div class="row">
            <div class="col s12 content_login">
                <form action="register/Login.php" method="POST">
                    <p class="p_login">Apelido:</p>
                    <input type="text" name="login" class="input_login">

                    <p class="p_senha">Senha:</p>
                    <input type="password" name="senha" class="input_login">

                    <a href="#">
                        <p class="p_esqueci">Esqueceu sua senha?</p>
                    </a>

                    <input type="submit" value="Entrar" class="waves-effect waves btn btn_login">
                    <a href="views/cadastro.php" class="criar_conta">Criar conta</a>

                    <?php
                    //if($_GET["erro"] == "loginOuSenhaIncorreta"){ ?>
                        <div onload="erroLogin();"></div>
                        <?php //} ?>
                </form>
            </div>
        </div>
    </div>
    <div class="col s12 footer">
        <p class="p_copyright">ver. 1.0.1 All rights are reserved.</p>
    </div>
</section>
<?php include_once 'layout/footer.php'; ?>