<?php 
session_start();
if (!isset($_SESSION["user"]) || $_SESSION["user"] != TRUE) { header("Location: http://localhost:8888/gottcha/"); }
$page = "page";
include_once '../layout/nav.php';
include_once '../register/Conn.php';
include_once '../register/User.php';
include_once '../register/ServiceUser.php';
$conn = new Conn("localhost","base_gotcha","root","root");

$u = new User;
$u->setId("1");

//usuario
$sername = new ServiceUser($conn, $u);
$usuario = $sername->listarUsuario();

error_reporting(0);
ini_set(“display_errors”, 0 );
?>
<section id="conquista">
    <div class="container">
        <div class="row">
            <div class="col s12 title_home">
                <h1>
                    <img src="../assets/images/logo.png" alt="logo" class="logo_login">
                </h1>
            </div>

            <div class="col s12 subtitle">
                <div class="col s4 line_white"></div>
                <?php foreach($usuario as $user){?>
                <div class="col s4 subtitle_login">Conquistas de
                    <?php echo $user['login'];?>
                </div>
                <?php } ?>
                <div class="col s4 line_white"></div>
            </div>
        </div>

        <div class="row">
            <div class="col 12 background-white">
                <?php foreach($usuario as $x9){  
                    if($x9['pontuacao'] >= 9){ ?>
                <div class="col s12 center">
                    <img src="../assets/images/medalhas/trophy3.png" alt="Medalha basica" class="img-medalhas">
                </div>
                <?php }else{  ?>
                <div class="col s12 center medalha_not_conquist">
                    <img src="../assets/images/medalhas/trophy3.png" alt="Medalha basica" class="img-medalhas">
                </div>
                <?php } } ?>
                <div class="col s12 center">
                    <?php foreach($usuario as $x8){   
                        if($x8['pontuacao'] >= 8){ ?>
                    <div class="col s3">
                        <img src="../assets/images/medalhas/medal5.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php }else{ ?>
                    <div class="col s4 medalha_not_conquist">
                        <img src="../assets/images/medalhas/medal5.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php } 
                    } 
                    foreach($usuario as $x7){   
                        if($x7['pontuacao'] >= 7){ ?>
                    <div class="col s3">
                        <img src="../assets/images/medalhas/medal5.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php }else{ ?>
                    <div class="col s3 medalha_not_conquist">
                        <img src="../assets/images/medalhas/medal5.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php } 
                    } 
                    foreach($usuario as $x6){   
                        if($x6['pontuacao'] >= 6){ ?>
                    <div class="col s3">
                        <img src="../assets/images/medalhas/trophy1.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php }else{?>
                    <div class="col s4 medalha_not_conquist">
                        <img src="../assets/images/medalhas/trophy1.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php } 
                    } 
                    foreach($usuario as $x5){   
                        if($x5['pontuacao'] >= 5){ ?>
                    <div class="col s3">
                        <img src="../assets/images/medalhas/trophy2.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php }else{ ?>
                    <div class="col s4 medalha_not_conquist">
                        <img src="../assets/images/medalhas/trophy2.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php } } ?>
                </div>

                <div class="col s12">
                    <?php foreach($usuario as $x4){   
                        if($x4['pontuacao'] >= 4){ ?>
                    <div class="col s3">
                        <img src="../assets/images/medalhas/medal1.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php }else{ ?>
                    <div class="col s3 medalha_not_conquist">
                        <img src="../assets/images/medalhas/medal1.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php } 
                    } 
                    foreach($usuario as $x3){  
                         if($x3['pontuacao'] >= 3){ ?>
                    <div class="col s3">
                        <img src="../assets/images/medalhas/medal2.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php }else{ ?>
                    <div class="col s3 medalha_not_conquist">
                        <img src="../assets/images/medalhas/medal2.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php } 
                } 
                foreach($usuario as $x2){   
                    if($x2['pontuacao'] >= 2){ ?>
                    <div class="col s3">
                        <img src="../assets/images/medalhas/medal3.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php }else{ ?>
                    <div class="col s3 medalha_not_conquist">
                        <img src="../assets/images/medalhas/medal3.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php } 
                    } 
                    foreach($usuario as $x1){  
                         if($x1['pontuacao'] >= 1){ ?>
                    <div class="col s3">
                        <img src="../assets/images/medalhas/medal4.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <?php }else{ ?>
                    <div class="col s3 medalha_not_conquist">
                        <img src="../assets/images/medalhas/medal4.png" alt="Medalha basica" class="img-medalhas">
                    </div>
                    <p class="alert-conquista">Você não fez nenhuma aula ainda!</p>
                    <?php } } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once '../layout/footer.php'; ?>