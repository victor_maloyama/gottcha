<?php 
$page = "page";
include_once '../layout/header.php';
?>
<section id="cadastro">
    <div class="container">
        <div class="row">
            <div class="col s12 title_login">
                <h1>
                    <img src="../assets/images/logo.png" alt="logo" class="logo_login">
                </h1>
            </div>

            <div class="col s12 subtitle">
                <div class="col s4 line_white"></div>
                <div class="col s4 subtitle_cadastro">Cadastro</div>
                <div class="col s4 line_white"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 content_cadastro">
                <form action="../register/Cadastro.php" method="POST">
                    <p class="p_login">Login:</p>
                    <input type="text" name="login"  class="input_cadastro">
                    
                    <p class="p_senha">E-mail:</p>
                    <input type="text" name="email" placeholder="exemplo@gmail.com" class="input_cadastro">
                    
                    <p class="p_senha">Senha:</p>
                    <input type="password" name="senha" class="input_cadastro">
                    <input type="submit" value="Cadastrar" class="waves-effect waves btn btn_cadastrar">
                </form>
            </div>
        </div>
    </div>
</section>
<?php include_once '../layout/footer.php'; ?>