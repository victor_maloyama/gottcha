<?php 
session_start();
if (!isset($_SESSION["user"]) || $_SESSION["user"] != TRUE) { header("Location: http://localhost:8888/gottcha/"); }
$page = "page";
include_once '../layout/nav.php';
include_once '../register/Conn.php';
include_once '../register/User.php';
include_once '../register/ServiceUser.php';
$conn = new Conn("localhost","base_gotcha","root","root");

$u = new User;
$u->setId("1");

$pontos = new ServiceUser($conn, $u);
$visualizar = $pontos->listarPontos();

error_reporting(0);
ini_set(“display_errors”, 0 );
?>
<section id="home">
    <div class="container">
        <div class="row">
            <div class="col s12 title_home">
                <h1>
                    <img src="../assets/images/logo.png" alt="logo" class="logo_login">
                </h1>
            </div>

            <div class="col s12 subtitle">
                <div class="col s4 line_white"></div>
                <div class="col s4 subtitle_login">Menu</div>
                <div class="col s4 line_white"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 content_home">
                <div class="col s12 btn_aulas">
                    <a href="aula1/conteudo-1.php">
                        <p class="title_btn">Aula 1</p>
                    </a>
                </div>
                <?php foreach($visualizar as $r){ 
                    if($r["pontuacao"] >= 2){ ?>
                <div class="col s12 btn_aulas">
                    <a href="aula1/perguntas.php">
                        <button class="title_btn">Aula 2</button>
                    </a>
                </div>
                <?php }else { ?>
                <div class="col s12 btn_aulas disable-btn">
                    <a href="aula1/perguntas.php">
                        <button class="title_btn disabled">Aula 2</button>
                    </a>
                </div>

                <?php } } foreach($visualizar as $re){ 
                     if($re["pontuacao"] >= 4){ ?>
                <div class="col s12 btn_aulas">
                    <a href="aula1/perguntas.php">
                        <button class="title_btn">Aula 3</button>
                    </a>
                </div>
                <?php }else { ?>
                <div class="col s12 btn_aulas disable-btn">
                    <a href="aula1/perguntas.php">
                        <button class="title_btn disabled">Aula 3</button>
                    </a>
                </div>
                <?php } } foreach($visualizar as $res){  
                    if($res["pontuacao"] >= 7){ ?>
                <div class="col s12 btn_aulas">
                    <a href="aula1/perguntas.php">
                        <button class="title_btn">Aula 4</button>
                    </a>
                </div>
                <?php }else { ?>
                <div class="col s12 btn_aulas <? echo 'disable-btn'; ?>">
                    <a href="aula1/perguntas.php">
                        <button class="title_btn" <? echo "disabled"; ?>>Aula 4</button>
                    </a>
                </div>
                <?php } } ?>
            </div>
        </div>
    </div>
</section>
<?php include_once '../layout/footer.php'; ?>