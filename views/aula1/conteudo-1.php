<?php 
$page = "conteudo";
include_once '../../layout/nav.php';
session_start();
if (!isset($_SESSION['user']) || $_SESSION['user'] != TRUE) { header("Location: http://localhost:8888/gottcha/"); }
?>
<section id="home">
    <div class="container">
        <div class="row">
            <div class="col s12 title_home">
                <h1>
                    <img src="../assets/images/logo.png" alt="logo" class="logo_gottcha">
                </h1>
            </div>

            <div class="col s12 subtitle">
                <div class="col s4 line_white"></div>
                <div class="col s4 subtitle_login">Menu</div>
                <div class="col s4 line_white"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 content_home">
                <div class="col s12 btn_aulas">
                    <a href="perguntas.php">
                        <p class="title_btn">Aula 1</p>
                    </a>
                </div>

                <div class="col s12 btn_aulas">
                    <a href="aula1/perguntas.php">
                        <p class="title_btn">Aula 2</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once '../../layout/footer.php'; ?>