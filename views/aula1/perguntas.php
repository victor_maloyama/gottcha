<?php 
session_start();
if (!isset($_SESSION["user"]) || $_SESSION["user"] != TRUE) { header("Location: http://localhost:8888/gottcha/"); }

$page = "conteudo";
include_once '../../layout/nav.php';
?>
<section id="perguntas-1">
	<div class="container">
		<div class="row">
			<form action="../../register/avaliar.php" method="POST">
				<div id="aula1_questao_1" class="question1">
					<p>Como se diz vermelho em inglês?</p>
					<div class="block_question1">
						<ul>
							<label id="pg1_q1">
								<li>
									<input name="answer-1" type="radio"> Red
								</li>
							</label>

							<label id="pg1_q2">
								<li>
									<input type="radio"> Yellow
								</li>
							</label>
						</ul>
						<ul>
							<label id="pg1_q3">
								<li>
									<input type="radio"> Blue
								</li>
							</label>

							<label id="pg1_q4">
								<li>
									<input type="radio"> Green
								</li>
							</label>
						</ul>
					</div>

					<div class="content-btn-prox" id="btn-proximo-1">
						<span>Próxima</span>
						<img src="../../assets/images/icons/next_arrow_icon.png" alt="proximo" class="img-proximo">
					</div>
				</div>

				<div id="aula1_questao_2" class="question2">
					<p>Como se diz amarelo em inglês?</p>
					<div class="block_question2">
						<ul>
							<label>
								<li id="pg2_q1">
									<input type="radio"> Red
								</li>
							</label>

							<label>
								<li id="pg2_q2">
									<input name="answer-2" type="radio"> Yellow
								</li>
							</label>
						</ul>
						<ul>
							<label>
								<li id="pg2_q3">
									<input type="radio"> Blue
								</li>
							</label>

							<label>
								<li id="pg2_q4">
									<input type="radio"> Green
								</li>
							</label>
						</ul>
					</div>

					<div class="content-btn-prox" id="btn-proximo-2">
						<span>Proximo</span>
						<img src="../../assets/images/icons/next_arrow_icon.png" alt="proximo" class="img-proximo">
					</div>
				</div>

				<div id="aula1_questao_3" class="question3">
					<p>Como se diz verde em inglês?</p>
					<div class="block_question3">
						<ul>
							<label>
								<li id="pg3_q1">
									<input type="radio"> Red
								</li>
							</label>

							<label>
								<li id="pg3_q2">
									<input type="radio"> Black
								</li>
							</label>
						</ul>
						<ul>
							<label>
								<li id="pg3_q3">
									<input name="answer-3" type="radio" type="radio"> Green
								</li>
							</label>

							<label>
								<li id="pg3_q4">
									<input type="radio"> Grey
								</li>
							</label>
						</ul>
					</div>

					<div class="content-btn-prox" id="btn-proximo-3">
						<span>Próximo</span>
						<img src="../../assets/images/icons/next_arrow_icon.png" alt="proximo" class="img-proximo">
					</div>
				</div>

				<div id="aula1_questao_4" class="question4">
					<p>Como se diz preto em inglês?</p>
					<div class="block_question4">
						<ul>
							<label>
								<li id="pg4_q1">
									<input type="radio">Red
								</li>
							</label>

							<label>
								<li id="pg4_q2">
									<input type="radio"> Blue
								</li>
							</label>
						</ul>

						<ul>
							<label>
								<li id="pg4_q3">
									<input name="answer-4" type="radio" type="radio"> Black
								</li>
							</label>

							<label>
								<li id="pg4_q4">
									<input type="radio"> Green
								</li>
							</label>
						</ul>
					</div>
					<input type="submit" value="enviar" class="btn_enviar_perguntas">
				</div>
			</form>
		</div>
	</div>
</section>

<?php include_once ("../../layout/footer.php") ?>