$(document).ready(function () {
    // =====AULA 1=====
    //validando os button da pergunta 1
    $('#perguntas-1 #pg1_q1 input').click(function () {
        $('.block_question1 #pg1_q1 li').toggleClass('active_answers');
        $('.block_question1 #pg1_q2 li').removeClass('active_answers');
        $('.block_question1 #pg1_q3 li').removeClass('active_answers');
        $('.block_question1 #pg1_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg1_q2 input').click(function () {
        $('.block_question1 #pg1_q2 li').toggleClass('active_answers');
        $('.block_question1 #pg1_q1 li').removeClass('active_answers');
        $('.block_question1 #pg1_q3 li').removeClass('active_answers');
        $('.block_question1 #pg1_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg1_q3 input').click(function () {
        $('.block_question1 #pg1_q3 li').toggleClass('active_answers');
        $('.block_question1 #pg1_q1 li').removeClass('active_answers');
        $('.block_question1 #pg1_q2 li').removeClass('active_answers');
        $('.block_question1 #pg1_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg1_q4 input').click(function () {
        $('.block_question1 #pg1_q4 li').toggleClass('active_answers');
        $('.block_question1 #pg1_q1 li').removeClass('active_answers');
        $('.block_question1 #pg1_q2 li').removeClass('active_answers');
        $('.block_question1 #pg1_q3 li').removeClass('active_answers');
    });

    //validando os button da pergunta 2
    $('#perguntas-1 #pg2_q1 input').click(function () {
        $('.block_question2 #pg2_q1 li').toggleClass('active_answers');
        $('.block_question2 #pg2_q2 li').removeClass('active_answers');
        $('.block_question2 #pg2_q3 li').removeClass('active_answers');
        $('.block_question2 #pg2_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg2_q2 input').click(function () {
        $('.block_question2 #pg2_q2 li').toggleClass('active_answers');
        $('.block_question2 #pg2_q1 li').removeClass('active_answers');
        $('.block_question2 #pg2_q3 li').removeClass('active_answers');
        $('.block_question2 #pg2_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg2_q3 input').click(function () {
        $('.block_question2 #pg2_q3 li').toggleClass('active_answers');
        $('.block_question2 #pg2_q1 li').removeClass('active_answers');
        $('.block_question2 #pg2_q2 li').removeClass('active_answers');
        $('.block_question2 #pg2_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg2_q4 input').click(function () {
        $('.block_question2 #pg2_q4 li').toggleClass('active_answers');
        $('.block_question2 #pg2_q1 li').removeClass('active_answers');
        $('.block_question2 #pg2_q2 li').removeClass('active_answers');
        $('.block_question2 #pg2_q3 li').removeClass('active_answers');
    });

    //validando os button da pergunta 3
    $('#perguntas-1 #pg3_q1 input').click(function () {
        $('.block_question3 #pg3_q1 li').toggleClass('active_answers');
        $('.block_question3 #pg3_q2 li').removeClass('active_answers');
        $('.block_question3 #pg3_q3 li').removeClass('active_answers');
        $('.block_question3 #pg3_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg3_q2 input').click(function () {
        $('.block_question3 #pg3_q2 li').toggleClass('active_answers');
        $('.block_question3 #pg3_q1 li').removeClass('active_answers');
        $('.block_question3 #pg3_q3 li').removeClass('active_answers');
        $('.block_question3 #pg3_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg3_q3 input').click(function () {
        $('.block_question3 #pg3_q3 li').toggleClass('active_answers');
        $('.block_question3 #pg3_q1 li').removeClass('active_answers');
        $('.block_question3 #pg3_q2 li').removeClass('active_answers');
        $('.block_question3 #pg3_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg3_q4 input').click(function () {
        $('.block_question3 #pg3_q4 li').toggleClass('active_answers');
        $('.block_question3 #pg3_q1 li').removeClass('active_answers');
        $('.block_question3 #pg3_q2 li').removeClass('active_answers');
        $('.block_question3 #pg3_q3 li').removeClass('active_answers');
    });

    //validando os button da pergunta 4
    $('#perguntas-1 #pg4_q1 input').click(function () {
        $('.block_question4 #pg4_q1 li').toggleClass('active_answers');
        $('.block_question4 #pg4_q2 li').removeClass('active_answers');
        $('.block_question4 #pg4_q3 li').removeClass('active_answers');
        $('.block_question4 #pg4_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg4_q2 input').click(function () {
        $('.block_question4 #pg4_q2 li').toggleClass('active_answers');
        $('.block_question4 #pg4_q1 li').removeClass('active_answers');
        $('.block_question4 #pg4_q3 li').removeClass('active_answers');
        $('.block_question4 #pg4_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg4_q3 input').click(function () {
        $('.block_question4 #pg4_q3 li').toggleClass('active_answers');
        $('.block_question4 #pg4_q1 li').removeClass('active_answers');
        $('.block_question4 #pg4_q2 li').removeClass('active_answers');
        $('.block_question4 #pg4_q4 li').removeClass('active_answers');
    });
    $('#perguntas-1 #pg4_q4 input').click(function () {
        $('.block_question4 #pg4_q4 li').toggleClass('active_answers');
        $('.block_question4 #pg4_q1 li').removeClass('active_answers');
        $('.block_question4 #pg4_q2 li').removeClass('active_answers');
        $('.block_question4 #pg4_q3 li').removeClass('active_answers');
    });

    //=====manipulando perguntas da aula 1=====
    $('#aula1_questao_2').addClass('hide');
    $('#aula1_questao_3').addClass('hide');
    $('#aula1_questao_4').addClass('hide');

    $('#btn-proximo-1').click(function () {
        $('#aula1_questao_2').removeClass('hide');
        $('#aula1_questao_1').addClass('hide');
        $('#aula1_questao_3').addClass('hide');
        $('#aula1_questao_4').addClass('hide');
    });
    $('#btn-proximo-2').click(function () {
        $('#aula1_questao_3').removeClass('hide');
        $('#aula1_questao_1').addClass('hide');
        $('#aula1_questao_2').addClass('hide');
        $('#aula1_questao_4').addClass('hide');
    });

    $('#btn-proximo-3').click(function () {
        $('#aula1_questao_4').removeClass('hide');
        $('#aula1_questao_1').addClass('hide');
        $('#aula1_questao_3').addClass('hide');
        $('#aula1_questao_2').addClass('hide');
    });
});