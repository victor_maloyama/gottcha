<?php include_once 'header.php'; ?>
<div id="menu">
  <div class="btn-menu">
    <span class="barra"></span>
    <div class="active-menu-blocks"></div>
  </div>
  <div class="block-menu">
    <div class="btn-menu-content">
      <div class="conteudo-menu">
        <a href="../views/conquistas.php">
          <span>
            <?php if($page == "conteudo"){?>
            <img src="../../assets/images/icons/conquistas_icon.jpg" alt="conquistas" id="icon_conquista" class="icon_menu">
            <?php }else{?>
            <img src="../assets/images/icons/conquistas_icon.jpg" alt="conquistas" id="icon_conquista" class="icon_menu">
            <?php } ?>
          </span>
          <span class="title-item"> Conquistas</span>
        </a>
      </div>
    </div>

    <div class="btn-menu-content">
      <div class="conteudo-menu">
        <a href="#">
          <span>
            <?php if($page == "conteudo"){?>
            <img src="../../assets/images/icons/configuracoes_icon.jpg" alt="Configuracoes" id="icon_configuracoes" class="icon_menu">
            <?php }else{?>
            <img src="../assets/images/icons/configuracoes_icon.jpg" alt="Configuracoes" id="icon_configuracoes" class="icon_menu">
            <?php } ?>
          </span>
          <span class="title-item"> Configurações</span>
        </a>
      </div>
    </div>

    <div class="btn-menu-content">
      <div class="conteudo-menu">
        <a href="#">
          <span>
            <?php if($page == "conteudo"){?>
            <img src="../../assets/images/icons/creditos_icon.jpg" alt="Creditos" id="icon_creditos" class="icon_menu">
            <?php }else{?>
            <img src="../assets/images/icons/creditos_icon.jpg" alt="Creditos" id="icon_creditos" class="icon_menu">
            <?php } ?>
          </span>
          <span class="title-item"> Créditos</span>
        </a>
      </div>
    </div>

    <div class="btn-menu-content">
      <div class="conteudo-menu">
        <a href="#">
          <span>
            <?php if($page == "conteudo"){?>
            <img src="../../assets/images/icons/sobre_icon.jpg" alt="Saiba mais" id="icon_saiba" class="icon_menu">
            <?php }else{?>
            <img src="../assets/images/icons/sobre_icon.jpg" alt="Saiba mais" id="icon_saiba" class="icon_menu">
            <?php } ?>
          </span>
          <span class="title-item"> Saiba mais</span>
        </a>
      </div>
    </div>

    <div class="btn-menu-content">
      <div class="conteudo-menu">
        <a href="#">
          <span>
            <?php if($page == "conteudo"){?>
            <img src="../../assets/images/icons/apoie_icon.jpg" alt="Apoie-nos" id="icon_apoie" class="icon_menu">
            <?php }else{?>
            <img src="../assets/images/icons/apoie_icon.jpg" alt="Apoie-nos" id="icon_apoie" class="icon_menu">
            <?php } ?>
          </span>
          <span class="title-item"> Apoie-nos</span>
        </a>
      </div>
    </div>

    <div class="btn-menu-content">
      <div class="conteudo-menu">
        <a href="http://localhost:8888/gottcha/" onclick="sair();">
          <span>
            <?php if($page == "conteudo"){?>
            <img src="../../assets/images/icons/logout_icon.jpg" alt="Sair" id="icon_sair" class="icon_menu">
            <?php }else{?>
            <img src="../assets/images/icons/logout_icon.jpg" alt="Sair" id="icon_sair" class="icon_menu">
            <?php } ?>
          </span>
          <span class="title-item"> Sair</span>
        </a>
      </div>
    </div>
  </div>
</div>