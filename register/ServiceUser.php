<?php 

class ServiceUser{

    private $db, $user;

    public function __construct(Conn $db, User $user){
        $this->db = $db->connect();
        $this->user = $user;
    }

    public function cadastrar(){
        $sql = "INSERT INTO tb_usuario (login, senha, email) VALUES (:login, :senha, :email)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":login", $this->user->getLogin());
        $stmt->bindValue(":senha", $this->user->getSenha());
        $stmt->bindValue(":email", $this->user->getEmail());
        $stmt->execute();   

        if(count($stmt) > 0){
            header("Location: http://localhost:8888/gottcha/");
        }else{
            header("Location: http://localhost:8888/gottcha/views/cadastro.php?create=erro");           
        }
    }

    public function login(){
        $sql = "SELECT * FROM tb_usuario WHERE login = :login AND senha = :senha";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":login", $this->user->getLogin());
        $stmt->bindValue(":senha", $this->user->getSenha());
        $stmt->execute();

        if(count($stmt) > 0){
            session_start();
            $_SESSION['user'] = $this->user->getLogin();
            header("Location: http://localhost:8888/gottcha/views/home.php");
        }else{
            unset($_SESSION['user']);
            header("Location: http://localhost:8888/gottcha/index.php?erro=loginOuSenhaIncorreta");
        }
    }

    public function salvarPontos(){
        $sql = "UPDATE tb_usuario SET pontuacao = :pontos WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":pontos", $this->user->getPontos());
        $stmt->bindValue(":id", $this->user->getId());
        $stmt->execute();
    }

    public function listarPontos(){
        $sql = "SELECT pontuacao FROM tb_usuario WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $this->user->getId());
        $stmt->execute();

        $res = $stmt->fetchAll(PDO::FETCH_ASSOC); 

        return $res;
    }

    public function listarUsuario(){
        $sql = "SELECT * FROM tb_usuario WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $this->user->getId());
        $stmt->execute();


        $show = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $show;
    }
}