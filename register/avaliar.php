<?php 

include_once ('Conn.php');
include_once ('User.php');
include_once ('ServiceUser.php');

$conn = new Conn("localhost","base_gotcha","root","root");

$q1 = $_POST["answer-1"];
$q2 = $_POST["answer-2"];
$q3 = $_POST["answer-3"];
$q4 = $_POST["answer-4"];

if($q1 == true && $q2 == true && $q3 == true && $q4 == true){
    $user = new User;
    $user->setId("1")->setPontos("4");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();
    
    echo "voce acertou as 4 perguntas";
 }else if($q1 == true && $q2 != true && $q3 != true && $q4 != true){
    $user = new User;
    $user->setId("1")->setPontos("1");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();

    echo "voce so acertou a primeira pergunta";
}else if($q1 != true && $q2 == true && $q3 != true && $q4 != true){
    $user = new User;
    $user->setId("1")->setPontos("1");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();

    echo "voce so acertou a segunda pergunta";
}else if($q1 != true && $q2 != true && $q3 == true && $q4 != true){
    $user = new User;
    $user->setId("1")->setPontos("1");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();

    echo "voce so acertou a terceira pergunta";
}else if($q1 != true && $q2 != true && $q3 != true && $q4 == true){
    $user = new User;
    $user->setId("1")->setPontos("1");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();

    echo "voce so acertou a quarta pergunta";
}else if($q1 == true && $q2 == true && $q3 != true && $q4 != true){
    $user = new User;
    $user->setId("1")->setPontos("2");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();
    
    echo "voce so acertou a primeira e a segunda pergunta";
}else if($q1 == true && $q2 != true && $q3 == true && $q4 != true){
    $user = new User;
    $user->setId("1")->setPontos("2");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();
    
    echo "voce so acertou a primeira e a terceira pergunta";
}else if($q1 == true && $q2 != true && $q3 != true && $q4 == true){
    $user = new User;
    $user->setId("1")->setPontos("2");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();
    
    echo "voce acertou a primeira e a quarta pergunta";
}else if($q1 != true && $q2 == true && $q3 == true && $q4 != true){
    $user = new User;
    $user->setId("1")->setPontos("2");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();

    echo "voce acertou a segunda e a terceira pergunta";
}else if($q1 != true && $q2 == true && $q3 != true && $q4 == true){
    $user = new User;
    $user->setId("1")->setPontos("2");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();
    
    echo "voce acertou a segunda e a quarta pergunta";
}else if($q1 != true && $q2 != true && $q3 == true && $q4 == true){
    $user = new User;
    $user->setId("1")->setPontos("2");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();
    
    echo "voce acertou a terceira e a quarta pergunta";
}else if($q1 != true && $q2 != true && $q3 != true && $q4 != true){
    $user = new User;
    $user->setId("1")->setPontos("0");

    $pontos = new ServiceUser($conn, $user);
    $pontos->salvarPontos();
    
    echo "voce errou tudo";
}else{
    echo "nao foi possivel avaliar as respostas";
}